package src.main.java;

import java.util.ArrayList;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
*   Represents a chat message
*/

public class Message {

  private String message;
  private String sender;
  private LocalDateTime timeStamp;
  private ArrayList<String> receivers;
  private MessageType messageType;

  public enum MessageType {
    TEXT, AUDIO, VIDEO
  }

  public Message(String msg, String sndr, LocalDateTime time, MessageType mType) {
    message = msg;
    sender = sndr;
    timeStamp = time;
    messageType = mType;
  }

  public boolean addReceiver(String receiver) {
    receivers.add(receiver);
    return true;
  }

  public String getTimeStamp() {
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    return dtf.format(timeStamp);
  }

  public String getMessage()          { return this.message;      }

  public String getSender()           { return this.sender;       }

  public MessageType getMessageType() { return this.messageType;  }

  public String getReceivers() {
    String rcv = "";
    for (int i = 0; i < receivers.size(); i++) {
        if (i == receivers.size() - 1) rcv += receivers.get(i); else rcv += receivers.get(i) + ",";
    }
    return rcv;
  }

}
