package src.main.java;

/** Chat client for CSC3002F assignment 1
* @author Jordy, Hendri, Jethro
* @version 0.1
* 5-03-18
*/

import java.net.Socket;
import java.util.Scanner;
import java.io.IOException;
import src.main.java.protocol.IChatServices;
import src.main.java.Session;
import src.main.java.Session.SessionType;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ChatClient implements Runnable, IChatServices {

  // TODO enable configurable host name
  private static final int PORT_NUMBER = 10000;
  private static final String HOST_NAME = "127.0.0.1";

  private static int usersInChat = 0;
  private boolean active;
  Scanner scanner;
  User chatUser;
  private Socket clientSocket;
  private PrintWriter out;
  private BufferedReader in;
  private Session session;

  public ChatClient(User user) {
    chatUser = user;
    try {
      session = new Session(SessionType.CLIENT);
      clientSocket = new Socket(HOST_NAME, PORT_NUMBER);
      scanner = new Scanner(System.in);
      out = new PrintWriter(clientSocket.getOutputStream(), true);
      in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }catch(IOException e) {
      e.printStackTrace();
    }
    usersInChat++;
    active = true;
  }

  /** Being extra. This allows users to post a poll into the chat. */
  public boolean postPoll() {
    return true;
  }

  /** Spawn new user thread */
  @Override
  public void run() {
    System.out.println("Client active...");
    String fromUser;
    while (active) {
      fromUser = scanner.nextLine();
      if (fromUser != null) {
        System.out.println("Client: " + fromUser);
        out.println(fromUser);
      }
    }
  }

  // TODO implement functionality

  /** Set up any connection-related resources. */
  @Override
  public void _establishConnection() {

  }

  /** Allows a user to send a message to a chat */
  @Override
  public boolean sendMessage(String message) {

    return true;
  }

  /** Determines whether the user is currently active in chat */
  @Override
  public boolean isActive() {
    return true;
  }

  /** Determines whether the client has an active internet connection */
  @Override
  public boolean hasInternetConnection() {
    return true;
  }

  /** perform any post-connection operations */
  @Override
  public void exit() {
    try {
      clientSocket.close();
    }catch(IOException e) {

    }
    usersInChat--;
  }

}
