package src.main.java;

import java.util.Scanner;

public class Configuration {

  /*
        ENUMERATIONS
  */

  public enum Agent {
    WINDOWS, LINUX, MAC
  }

  public enum ChatAppType {
    CLIENT, SERVER
  }

  public enum ConfigurationType {
    DEFAULT, CUSTOM
  }

  /*
        MEMBERS
  */

  Agent userAgent;
  ChatAppType chatAppType;
  ConfigurationType configType;

  /*
        CONSTRUCTORS
  */

  public Configuration(ConfigurationType config, ChatAppType chatType) {
    switch (config) {
      case DEFAULT: {
        userAgent = Agent.LINUX;
        chatAppType = chatType;
        configType = config;
        break;
      }

      case CUSTOM: {
        /* CLI to allow for custom options */
        break;
      }

    }
  }

  /*
        METHODS
  */

  /** Save changes as default */
  public void applyConfiguration() {

  }

}
