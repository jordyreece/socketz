package src.main.java;

/**
* Stores all user information
*/

import java.util.ArrayList;
import java.time.LocalDateTime;

public class User {

  private static ArrayList<String> userStatus = new ArrayList<String>() {{
      add("Available");
      add("Busy");
      add("In a meeting");
  }};

  private String name;
  private String surname;
  private int age;
  private String userName;
  private String emailAddress;
  private LocalDateTime lastActiveTime;


  public User() {

  }

  public User(String name, String surname, int age, String userName, String emailAddress) {
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.userName = userName;
    this.emailAddress = emailAddress;
    lastActiveTime = LocalDateTime.now();
  }

  public void addStatusMessage(String msg) {
    userStatus.add(msg);
  }

  /*
        ACCESS
  */

  public String getUserName               () {    return userName;        }
  public String getEmailAddress           () {    return emailAddress;    }
  public LocalDateTime getLastActiveTime  () {    return lastActiveTime;  }
  public void isOnline() {
    lastActiveTime = LocalDateTime.now();
  }

  /*
        MUTATE
  */

  // TODO: Add validation checks

  public void setName (String name) {
    this.name = name;
  }

  public void setSurname(String sname) {
    this.surname = sname;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public void setUserName(String uname) {
    this.userName = uname;
  }

  public void setEmailAddress(String email) {
    this.emailAddress = email;
  }

  @Override
  public String toString() {
    return name +","+surname+","+age+","+userName+","+emailAddress;
  }
}
