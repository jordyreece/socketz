package src.main.java;

import src.main.java.Configuration.ConfigurationType;
import src.main.java.Configuration.ChatAppType;
import src.main.java.Configuration;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import src.main.java.security.SecurityTools;
import java.lang.Integer;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
/** Driver for Chat App
* @author Jordy, Hendri, Jethro
* 05-03-18
*/

public class Main {

  /*
  CONSTANTS
  */

  private static final double VERSION_NO = 0.1;
  private static final String APPLICATION_NAME = "CSC3002F Assignment 1 - Socket Programming";

  /*
  MEMBERS
  */

  private static Configuration config;

  public static void main (String [] args) {

    System.out.println(APPLICATION_NAME);
    System.out.println("Version " + VERSION_NO);

    String settingsConfiguration = "";
    String processType = "";
    User user = new User();
    try {
      /* ENTER CHAT APP SETTINGS */
      BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
      System.out.print("Enter settings configuration (d/c): ");
      settingsConfiguration = in.readLine();
      settingsConfiguration.trim();
      System.out.println("Selected configuration: " +settingsConfiguration);
      System.out.print("Enter process type (c/s): ");
      processType = in.readLine();
      processType.trim();
      System.out.println("Selected process type: "+processType);

      /* Authenticate user profile */
      /* ENTER USER DETAILS IF CLIENT PROCESS */
      if (processType.contains("c")) {
        SecurityTools securityTools = new SecurityTools();
        System.out.println("Are you a new user? (y/n): ");
        String isNewUser = in.readLine();
        if (isNewUser.contains("y")) {
          String name, surname, age, username, email;
          System.out.println("Enter name: ");
          name = in.readLine();
          System.out.println("Enter surname: ");
          surname = in.readLine();
          System.out.println("Enter age: ");
          age = in.readLine();
          System.out.println("Enter username: ");
          username = in.readLine();
          System.out.println("Enter email address: ");
          email = in.readLine();

          // populate user object
          user.setName(name);
          user.setSurname(surname);
          user.setAge(Integer.parseInt(age));
          user.setUserName(username);
          user.setEmailAddress(email);

          // add profile to text file
          try(FileWriter fw = new FileWriter("profiles.txt", true);
          BufferedWriter bw = new BufferedWriter(fw);
          PrintWriter out = new PrintWriter(bw))
          {
            out.println(user.toString());
            out.close();
          } catch (IOException e) {
            //exception handling left as an exercise for the reader
          }

        } else if (isNewUser.contains("n")) {
          System.out.println("Enter username: ");
          String username = in.readLine();
          username.trim();
          while( (user = securityTools.authenticateUser(username)) == null ) {
            System.out.println("Invalid username entered. Please try again.");
            System.out.println("Enter username: ");
            username = in.readLine();
            username.trim();
          }
        }else { // handle any fringe cases here in terms of IO
                // i.e. user enters incorrect information
        }
      }
    } catch(Exception e) {
      System.out.println("Fatal error occurred. System exiting...");
      System.exit(1);
    }

      /* default configuration for clients */
    if (settingsConfiguration.contains("d") && processType.contains("c")) {
      config = new Configuration(ConfigurationType.DEFAULT, ChatAppType.CLIENT);

      /* default server configuration */
    } else if(settingsConfiguration.contains("d") && processType.contains("s")) {
      config = new Configuration(ConfigurationType.DEFAULT, ChatAppType.SERVER);

      /* custom client configuration */
    } else if(settingsConfiguration.contains("c") && processType.contains("c")) {
      config = new Configuration(ConfigurationType.CUSTOM, ChatAppType.CLIENT);

      /* custom server configuration */
    }else if(settingsConfiguration.contains("c") && processType.contains("s")) {
      config = new Configuration(ConfigurationType.CUSTOM, ChatAppType.SERVER);
    }else {
      System.out.println("Unrecognised command entered. Exiting system...");
      System.exit(1);
    }

    switch (config.chatAppType) {
      case CLIENT: {
        System.out.println("Starting client...");
        Thread userThread = new Thread(new ChatClient(user));
        userThread.start();
        break;
      }

      case SERVER: {
        System.out.println("Server running...");
        ChatServer chatServer = new ChatServer();
        chatServer.run();
        break;
      }
    }

  }
}
