package src.main.java.protocol;

/**
* Set of services that must be implemented by every user in the chat app
*/

// TODO: add in more default services
public interface IChatServices {

    public boolean hasInternetConnection();
    public void _establishConnection();
    public boolean sendMessage(String message);
    public boolean isActive();
    public void exit();
}
