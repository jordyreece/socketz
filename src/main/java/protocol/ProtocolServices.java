package src.main.java.protocol;

import src.main.java.Message;

public interface ProtocolServices {

  public enum ProtocolStatus {
    WAITING, SENT, DELIVERED
  }

  public boolean _construct();
  public boolean send(String msg);
  public boolean status();
}
