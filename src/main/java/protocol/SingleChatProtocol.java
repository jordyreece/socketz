package src.java.main.protocol;

/**
* Protocol for 1-1 Chat
*/

import src.main.java.Message;
import src.main.java.Message;
import src.main.java.protocol.ProtocolServices;
import src.main.java.security.SecurityTools;

public class SingleChatProtocol implements ProtocolServices {

  private Message message;
  private String formattedMessage;
  private SecurityTools securityTools;

  public SingleChatProtocol(Message msg) {
    securityTools = new SecurityTools();
  }

  /**
  * server periodically pings server for updates
  * limit file size
  *
  */

  /** Defines message protocol */
  public boolean _construct() {
    formattedMessage =
                        // define header
                       "SENDER: "       + message.getSender()                         + "\n"
                       + "TIME: "       + message.getTimeStamp()                      + "\n"
                       + "RECEIVERS: "  + message.getReceivers()                      + "\n"
                       + "HASH: "       + securityTools.SHA256(message.getMessage())  + "\n"
                       // define message
                       + "ECHO: "       + message.getMessage();
    return true;
  }

  public boolean send(String msg) {
    return true;
  }

  public boolean status() {
    return true;
  }

}
