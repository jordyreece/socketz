package src.main.java;

/**
*   Tracks a single session between clients, single chat or multiple
*/

import java.util.ArrayList;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Session {

  private ArrayList<String> participants;
  private int messagesSent;
  private LocalDateTime sessionBegin;
  private LocalDateTime sessionEnd;
  private SessionType sessionType;

  public enum SessionType {
    CLIENT, SERVER
  }

  public Session (SessionType sType) {
    switch (sType) {
      case CLIENT: {
        participants = new ArrayList<>();
        messagesSent = 0;
        sessionBegin = LocalDateTime.now();
        sessionType = sType;
        break;
      }
      case SERVER: {

      }
    }
  }

  public void addParticipant(String participant) {
    participants.add(participant);
  }

  public void incrementMessageCount() {
    messagesSent++;
  }

  public void endSession() {
    sessionEnd = LocalDateTime.now();
    // TODO add session duratio
  }

  public void printSessionInformation() {
    switch (sessionType) {
        case CLIENT: {
          System.out.println("Leaving conversation...");
          System.out.println("Participants: ");
          for(String person: participants) {
            System.out.println(person);
          }
          DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
          System.out.println("Session begin:\t\t" +     dtf.format(sessionBegin));
          System.out.println("Session end:\t\t"   +     dtf.format(sessionEnd));
          break;
        }
      case SERVER: {
        // print server session information
        break;
      }
    }
  }

}
