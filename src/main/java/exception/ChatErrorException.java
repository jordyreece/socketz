package src.main.java;

/** Parent chat error exception
* @author Jordy
* @version 0.1
*/

import java.lang.Throwable;

/* implement standard methods */
public class ChatErrorException extends Throwable {

}
