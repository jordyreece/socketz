package src.main.java.security;

import java.security.MessageDigest;
import java.nio.charset.StandardCharsets;
import java.lang.Exception;
import java.io.IOException;
import java.util.Scanner;
import src.main.java.User;
import java.lang.Integer;
/**
* Primitive obfuscation tool for String messages
*/

public class SecurityTools {



  public SecurityTools() { }

  /** One-way hash function to ensure message is not compromised */
  public String SHA256(String message) {
    try {
      MessageDigest digest = MessageDigest.getInstance("SHA-256");
      byte[] hash = digest.digest(message.getBytes(StandardCharsets.UTF_8));
      return new String(hash, "UTF-8");
    }catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  /** Determine if message was altered by comparing the received hash to the hash of the message */
  public boolean verifyHash(String receivedMessage, String sha256Hash) {
    return this.SHA256(receivedMessage.trim()).equals(sha256Hash.trim()); // ensure no whitespace
  }

  /** Check whether user exists on system */
  public User authenticateUser(String username) {
    String [] credentials;
    if ( (credentials = checkUserExists(username)) != null ) {
      User user = new User();
      user.setName(credentials[0]);
      user.setSurname(credentials[1]);
      user.setAge(Integer.parseInt(credentials[2]));
      user.setUserName(credentials[3]);
      user.setEmailAddress(credentials[4]);
      return user;
    }else {
      return null;
    }
  }

  /** Determines whether user has a profile on the system */
  public String[] checkUserExists(String username) {
    try {
      Scanner sc = new Scanner("profiles.txt");
      while(sc.hasNext()) {
        String line  = sc.nextLine();
        String [] data = line.split(",");
        if (data[3].trim().equals(username.trim())) {
          return data;
        }
      }
    } catch(Exception e) {
      // e.printStackTrace();
    }
    return null;
  }

}
