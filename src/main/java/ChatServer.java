package src.main.java;

/** Chat server for CSC3002F assignment 1
* @author Jordy, Hendri, Jethro
* @version 0.1
* 5-03-18
*/

import java.net.ServerSocket;
import java.net.Socket;
import java.lang.Throwable;
import java.lang.Exception;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import src.main.java.Session;
import java.net.InetAddress;

public class ChatServer {

  private static final int PORT_NUMBER = 10000;
  private ServerSocket serverSocket;
  private Socket socket;
  private boolean active;
  private PrintWriter out;
  private BufferedReader in;
  private String line;
  private Session session;

  public ChatServer() {
    try {
      serverSocket = new ServerSocket(PORT_NUMBER);
      socket = serverSocket.accept();
      out = new PrintWriter(socket.getOutputStream(), true);
      in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    } catch(IOException e) {
      e.printStackTrace();
    }
    active = true;
  }

  /** Any network-related initiation */
  public void _init() {

  }

  /** Determine if internet connection is available */
  public boolean checkInternetConnection() {
    try{
      InetAddress address  = InetAddress.getByName("https://www.google.com"); // ping google.com
      return address.isReachable(2000);
    }catch(IOException e) {
      return false;
    }
  }

  /** Runs the chat server */
  public void run() {
    System.out.println("Server active...");
    String inputLine, outputLine;
    outputLine = "Hello"; // TODO communicate using chat protocol here
    try {
      while ((inputLine = in.readLine()) != null) {
        System.out.println(inputLine);
        // specify which clients to send to
        out.println(outputLine); //
      if (outputLine.equals("Bye."))
          break;
      }
    }catch(IOException e) {
        e.printStackTrace();
    }

    implode();
  }

  /** Kaboom. */
  public void implode() {
    try {
      serverSocket.close();
      in.close();
      out.close();
    }catch(IOException e) {
      e.printStackTrace();
    }
  }
}
